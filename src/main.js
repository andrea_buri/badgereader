import Vue from 'vue'
import App from './App.vue'
import router from './router';

import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

var send = function () {
  console.log('SEND')
}

var myMixin = {
  created: function () {
  },
  methods: {
  }
}

Vue.config.productionTip = false

new Vue({
  render: h=>h(App),
  router,
  mixins: [myMixin]
}).$mount('#app')
