const Database = require('better-sqlite3');

class Sqlite3Backend{
    constructor(dbfile=""){
        if (dbfile){
            this.db = new Database(dbfile);
        }
        else{
            this.db = new Database(':memory:');
        }        
    }
    register(meta){
        if (!this.tableExists(meta.refTable)){
            this.createTable(meta)
        }
    }
    dropAndRegister(meta){
        this.dropTable(meta);
        this.createTable(meta);
    }
    tableExists(refTable){
        let stmt = this.db.prepare(`SELECT name FROM sqlite_master WHERE type='table' AND name='${refTable}'`);
        return stmt.get()
    }
    dropTable(meta){
        let drop_stmt = `DROP TABLE IF EXISTS '${meta.refTable}'`;
        this.db.exec(drop_stmt);
    }
    createTable(meta){
        let create_stmt = `
            CREATE TABLE '${meta.refTable}' (
                ID TEXT NOT NULL,
                ${meta.cols_schema}`;
        meta.foreignKeyMetas.forEach((fkey)=>{
            create_stmt += `,
                ${fkey.refTable}_ID INTEGER`
        })      
        create_stmt += `,
            PRIMARY KEY(ID)`
        meta.foreignKeyMetas.forEach((fkey)=>{
            create_stmt += `,
                CONSTRAINT fk_${fkey.refTable}
                FOREIGN KEY (${fkey.refTable}_ID)
                REFERENCES ${fkey.refTable}(ID)
            `
        })
        create_stmt += ');'
        this.db.exec(create_stmt);
    }
    createOrUpdate(persistable){
        debugger
        let create_stmt = this.db.prepare(`INSERT INTO ${persistable.meta.refTable.name} (${persistable.meta.cols}) VALUES(${persistable.meta.snail});`);
        let update_stmt = this.db.prepare(`UPDATE ${persistable.meta.refTable.name} SET ${persistable.meta.cols_snail} WHERE ID='${persistable.key}';`);

        let d = {
            ID: persistable.key,
        }
        for (let field of persistable.meta.fields){
            d[field] = persistable[field]
        }
        if (persistable.fkey){
            d[persistable.meta.fkeyName] = persistable.fkey
        }
        try {
            return create_stmt.run(d);
        } catch (error) {
            return update_stmt.run(d);
        }
    }
    deleteAll(meta){
        let stmt = this.db.prepare(`DELETE FROM '${meta.refTable}';`);
        stmt.run();
    }
    readAll(meta){
        if (!meta){
            throw new Error("missing meta for read")
        }
        let stmt = this.db.prepare(`SELECT * FROM '${meta.refTable}';`);
        return stmt.all();
    }

}
module.exports = {
    Sqlite3Backend
};