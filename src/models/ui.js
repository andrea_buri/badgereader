//there's no interface construct in JS/ES6 build a pseudo interface by class aggregation
class Rowable{
    initializer ()     {}
    get rowable(){
        return {
            "fields": this.rowable_fields,
            "meta": this.rowable_meta,
            "children": this.rowable_children,
            "show": this.rowable_show,
            "hash": this.rowable_hash    
        }
    }
    get rowable_fields(){
        throw "subclass should provide property A"
    }
    get rowable_meta(){
        throw "subclass should provide property B"
    }
    get rowable_children(){
        throw "subclass should provide property C"
    }
    get rowable_show(){
        throw "subclass should provide property D"
    }
}

module.exports = {
    Rowable
}