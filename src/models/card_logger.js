const { Card } = require( "./card");
const { BadgeLog } = require( "./badge_log");
const { Sqlite3Backend } = require("./persister_sqlite3")

class CardLogger{
    constructor(){
        this.be = new Sqlite3Backend("foo.db")
        this.be.register(BadgeLog.persistable_meta)
    }
    log(cardNumber, time){
        let badgeLog = BadgeLog.fromRaw(
            {
                "raw": JSON.stringify({
                    'time': time,
                    'badge_nr': cardNumber
                })
            }
        )
        this.be.createOrUpdate(badgeLog.persistable)
    }
    showLogs(fake=false){
        console.log("showlogs", fake)
        if (fake){
            this.log("123", "2021-02-13T09:16:38.066Z")
        }
        return Promise.resolve(
            this.be.readAll(BadgeLog.persistable_meta)
            .map((log)=>{
                console.log(log)
                return {time: JSON.parse(log.raw).raw.time, "name":"mino"}
            })
        )
    }
}

module.exports = {
    CardLogger
}