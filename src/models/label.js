const aggregation = require('./helpers/aggregation')

const { Rowable } = require("./ui")
const export_fields = require("./export_fields")
const {
    PersistableMeta,
    ExportableMeta,
    Exportable,
    KeyMeta,
    RefTable,
    ForeignKeyMeta,
    Persistable} = require('./persister');

class BaseModel{
    constructor(){
        this.children = []
        this.parent = null
        this._raw = null
        this.children_class = null
    }  
    
    static fromRaw(raw, parent=null){
        throw "subclass should provide property"
    }
}

class PersistableRowableExportableBaseModel extends aggregation(BaseModel, Persistable, Rowable, Exportable){
    get rowable_children(){
        return this.children.map((child)=>{return child.rowable})
    }
    get persistable_raw(){
        return this._raw
    }
    get persistable_fkey(){
        if (this.parent){
            return this.parent.persistable_key
        }
    }
    get persistable_key(){
        return JSON.parse(this._raw).id
    }    
    save(){
        this.constructor.persister.createOrUpdate(this.persistable)
        for(let child of this.children){
            child.save()
        }
    }
    export(){
        this.constructor.exporter.deleteAll(this.exportable.meta)
        if(this.parent)
            this.parent.export()
        this.constructor.exporter.createOrUpdate(this.exportable)
    }
    static set_persister(val){
        this.persister = val
        this.persister.register(this.persistable_meta)
        if(this.children_class){
            this.children_class.set_persister(val)
        }
    }
    static set_exporter(val){
        this.exporter = val
        if(this.children_class){
            this.children_class.set_exporter(val)
        }
        this.exporter.dropAndRegister(this.exportable_meta)
        if(this.children_class){
            this.children_class.set_exporter(val)
        }
    }

    get rowable_show(){
        return JSON.parse(this._raw)
    }
    get rowable_children(){
        return this.children.map((child)=>{return child.rowable})
    }
    get rowable_meta(){
        return this.constructor.rowable_meta;
    }
    get rowable_hash(){
        return JSON.parse(this._raw).id
    }
    static deleteAll(){
        if(this.children_class){
            this.children_class.deleteAll()
        }
        this.persister.deleteAll(this.persistable_meta)
    }
    static readAll(parent_cb = null){
        let instances = this.persister.readAll(this.persistable_meta).map((row)=>{
            let instance = new this();
            instance._raw = row.raw
            if (parent_cb){
                parent_cb(instance, row)
            }
            return instance; 
        })
        if(this.children_class){
            this.children_class.readAll((child, child_row)=>{
                let parent = instances.filter((instance)=>{return child_row[child.persistable_meta.fkeyName] == instance.persistable_key})[0]
                parent.children.push(child)
                child.parent = parent
            })    
        }
        return instances;
    }  
    static readByFilter(cb){
        let instances = this.readAll()
        return instances.filter(cb)[0]
    }  
}

class Label extends PersistableRowableExportableBaseModel{
    static fromRaw(raw, parent=null){
        let instance = new this();
        instance._raw = JSON.stringify(raw)
        let children = raw["packs"]
        instance.children = children.map((child_raw)=>{
            let child = Pack.fromRaw(child_raw, instance);
            return child
        })    
        return instance
    }
    get persistable_key(){
        return JSON.parse(this._raw).main_info.id
    }
    get rowable_hash(){
        return JSON.parse(this._raw).main_info.id
    }

    static get children_class(){
        return Pack
    }
    //persistable    
    static get exportable_meta(){
        return new ExportableMeta(
            new RefTable("labels"),
            new KeyMeta("ID"),
            null,
            ["raw",...export_fields["labels"]]
        )
    }
    get exportable_meta(){
        return Label.exportable_meta;
    }    
    static get persistable_meta(){
        return new PersistableMeta(
            new RefTable("labels"),
            new KeyMeta("ID"),
            null
        )
    }
    get persistable_meta(){
        return Label.persistable_meta;
    }

    //rowable
    get rowable_fields(){
        return [JSON.parse(this._raw).main_info.id, JSON.parse(this._raw).main_info.name]
    }

    static get rowable_meta(){
        return {
            "name":"labels",
            "headers":["ID","Nome"],
            "children_name":"packs",
            "children":{
                "meta": Pack.rowable_meta
            }
        }
    }
    get packs(){
        return this.children;
    }
}

class Pack extends PersistableRowableExportableBaseModel{
    static fromRaw(raw, parent=null){
        if (!parent){
            throw "A parent label is required to build a pack"
        }
        let instance = new this();
        instance._raw = JSON.stringify(raw)
        instance.parent = parent
        return instance
    } 

    //rowable
    get rowable_fields(){
        return [JSON.parse(this._raw).id, JSON.parse(this._raw).ean, JSON.parse(this._raw).pack_name]
    }
    static get rowable_meta(){
        return {
            "name":"packs",
            "headers":["ID","Codice EAN","Nome"],
            "children_name":null,
            "children":null,
        }
    }
    get rowable_show(){
        let rawdata  = JSON.parse(this._raw)
        return {
            "Nome":rawdata["pack_name"],
            "Nome vendita":rawdata["sales_name"],
            "Codice EAN":rawdata["ean"],
            "Istruzioni d'uso":rawdata["operational_rules"],
            "Shelf life(giorni)":rawdata["shelf_life"]
        }
    }
    //persistable
    static get exportable_meta(){
        return new ExportableMeta(
            new RefTable("packs"),
            new KeyMeta("ID"),
            new ForeignKeyMeta("labels", "ID", "labels_ID"),
            [...export_fields["packs"]]
        )
    }
    get exportable_meta(){
        return Pack.exportable_meta;
    } 
          
    static get persistable_meta(){
        return new PersistableMeta(
            new RefTable("packs"),
            new KeyMeta("ID"),
            new ForeignKeyMeta("labels", "ID", "labels_ID")
        )
    }
    get persistable_meta(){
        return Pack.persistable_meta
    }
}

module.exports = {
    Label,
    Pack
}