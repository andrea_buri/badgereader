const aggregation = require('./helpers/aggregation')
const { v4 } = require("uuid")
const { Rowable } = require("./ui")
const export_fields = require("./export_fields")
const {
    PersistableMeta,
    ExportableMeta,
    Exportable,
    KeyMeta,
    RefTable,
    ForeignKeyMeta,
    Persistable} = require('./persister');

class BaseModel{
    constructor(){
        this.children = []
        this.parent = null
        this._raw = null
        this.children_class = null
    }  
    
    static fromRaw(raw, parent=null){
        throw "subclass should provide property"
    }
}

class PersistableRowableExportableBaseModel extends aggregation(BaseModel, Persistable, Rowable, Exportable){
    get rowable_children(){
        return this.children.map((child)=>{return child.rowable})
    }
    get persistable_raw(){
        return this._raw
    }
    get persistable_fkey(){
        if (this.parent){
            return this.parent.persistable_key
        }
    }
    get persistable_key(){
        return JSON.parse(this._raw).id
    }    
    save(){
        debugger;
        this.constructor.persister.createOrUpdate(this.persistable)
        for(let child of this.children){
            child.save()
        }
    }
    export(){
        this.constructor.exporter.deleteAll(this.exportable.meta)
        if(this.parent)
            this.parent.export()
        this.constructor.exporter.createOrUpdate(this.exportable)
    }
    static set_persister(val){
        debugger;
        this.persister = val
        this.persister.register(this.persistable_meta)
        if(this.children_class){
            this.children_class.set_persister(val)
        }
    }
    static set_exporter(val){
        this.exporter = val
        if(this.children_class){
            this.children_class.set_exporter(val)
        }
        this.exporter.dropAndRegister(this.exportable_meta)
        if(this.children_class){
            this.children_class.set_exporter(val)
        }
    }

    get rowable_show(){
        return JSON.parse(this._raw)
    }
    get rowable_children(){
        return this.children.map((child)=>{return child.rowable})
    }
    get rowable_meta(){
        return this.constructor.rowable_meta;
    }
    get rowable_hash(){
        return JSON.parse(this._raw).id
    }
    static deleteAll(){
        if(this.children_class){
            this.children_class.deleteAll()
        }
        this.persister.deleteAll(this.persistable_meta)
    }
    static readAll(parent_cb = null){
        let instances = this.persister.readAll(this.persistable_meta).map((row)=>{
            let instance = new this();
            instance._raw = row.raw
            if (parent_cb){
                parent_cb(instance, row)
            }
            return instance; 
        })
        if(this.children_class){
            this.children_class.readAll((child, child_row)=>{
                let parent = instances.filter((instance)=>{return child_row[child.persistable_meta.fkeyName] == instance.persistable_key})[0]
                parent.children.push(child)
                child.parent = parent
            })    
        }
        return instances;
    }  
    static readByFilter(cb){
        let instances = this.readAll()
        return instances.filter(cb)[0]
    }  
}

class BadgeLog extends PersistableRowableExportableBaseModel{
    static fromRaw(raw, parent=null){
        let instance = new this();
        instance._raw = JSON.stringify(raw)
        instance._generated_id = v4()
        return instance
    }

    get persistable_key(){
        return this._generated_id
    }

    get rowable_hash(){
        return this._generated_id
    }

    static get children_class(){
        return null
    }
    //persistable    
    static get exportable_meta(){
        return new ExportableMeta(
            new RefTable("badge_logs"),
            new KeyMeta("ID"),
            null,
            ["raw"]
        )
    }
    get exportable_meta(){
        return BadgeLog.exportable_meta;
    }    
    static get persistable_meta(){
        return new PersistableMeta(
            new RefTable("badge_logs"),
            new KeyMeta("ID"),
            null
        )
    }
    get persistable_meta(){
        return BadgeLog.persistable_meta;
    }

    //rowable
    get rowable_fields(){
        return [
            JSON.parse(JSON.parse(this._raw).raw).time,
            JSON.parse(JSON.parse(this._raw).raw).badgeNr
        ]
    }
    get time(){
        return JSON.parse(this._raw).time
    }
    get badgeNr(){
        return JSON.parse(this._raw).badgeNr
    }
}

class Badge extends PersistableRowableExportableBaseModel{
    static fromRaw(raw, parent=null){
        let instance = new this();
        instance._raw = JSON.stringify(raw)
        return instance
    }

    get persistable_key(){
        return JSON.parse(this._raw).badgeNr
    }

    get rowable_hash(){
        return this.persistable_key
    }

    static get children_class(){
        return null
    }
    //persistable    
    static get exportable_meta(){
        return new ExportableMeta(
            new RefTable("badges"),
            new KeyMeta("ID"),
            null,
            ["raw"]
        )
    }
    get exportable_meta(){
        return BadgeLog.exportable_meta;
    }    
    static get persistable_meta(){
        return new PersistableMeta(
            new RefTable("badges"),
            new KeyMeta("ID"),
            null
        )
    }
    get persistable_meta(){
        return Badge.persistable_meta;
    }

    //rowable
    get rowable_fields(){
        return [
            JSON.parse(this._raw).firstName,
        ]
    }

    get firstName(){
        return JSON.parse(this._raw).firstName
    }
    get lastName(){
        return JSON.parse(this._raw).lastName
    }
    get holderNumber(){
        return JSON.parse(this._raw).holderNumber
    }
    get badgeNr(){
        return JSON.parse(this._raw).badgeNr
    }
}

module.exports = {
    BadgeLog,
    Badge
}