const { Fetcher } = require('../fetcher')
const mocklabel = require("./mocklabel")
const mocklabeldetail = require("./mocklabeldetail")

class MockFetcher extends Fetcher{
    constructor(max_response_size = 50){
        super()
        this.response_size = this.randomFromRange(5, max_response_size)
        this.label_cnt = 1
        this.pack_cnt = 1
        this.company_cnt = 1
    }
    mockCompany(){
        this.company_cnt += 1;
        return {
            "id": this.company_cnt,
            "name": `Company ${this.company_cnt}`
        }
    }
    randomFromRange(min, max){
        return min + Math.floor(Math.random() * (max - min));
    }
    _populateMock(mock, n, p=0){
        let mock_str = JSON.stringify(mock)
        mock_str = mock_str.replace(/<N>/g, n)
        mock_str = mock_str.replace(/-999/g, n)
        mock_str = mock_str.replace(/<P>/g, p)
        mock_str = mock_str.replace(/-888/g, p)
        return JSON.parse(mock_str)
    }
    mockLabel(mockt, id=null){
        let mockl = Object.assign({}, mockt)
        let mock_pack = mockl["packs"][0]
        mockl["packs"] = []
        let _id = null
        if(!id){
            _id = this.label_cnt
            this.label_cnt += 1
        }
        else{
            _id = id
        }        
        mockl = this._populateMock(mockl, _id)
        let n_packs = this.randomFromRange(0, 10)
        for(let i = 0; i < n_packs; i++){
            this.pack_cnt += 1
            let newpack = this._populateMock(mock_pack, _id, this.pack_cnt)
            mockl["packs"].push(newpack)
        }
        return mockl
    }
    mockLabelNoDetail(){        
        return this.mockLabel(mocklabel.result)
    }
    mockLabelDetail(id = null){
        return this.mockLabel(mocklabeldetail.result, id)
    }

    mockCompanyList(){
        let companies = []
        for (let index = 0; index < 5; index++) {
            companies.push(this.mockCompany())
        }
        return companies;
    }

    mockLabelList(){
        let labels = []
        for (let index = 0; index < this.response_size; index++) {
            labels.push(this.mockLabelNoDetail())
        }
        return labels;
    }
    getCompanies(){
        return new Promise(async (resolve, reject) => {            
            resolve(this.mockCompanyList());    
        })
    }
    getLabels(){
        return new Promise(async (resolve, reject) => {            
            setTimeout(()=>{
                resolve(this.mockLabelList());    
            }, 1500)         
        })
    }
    getLabel(label){
        return new Promise(async (resolve, reject) => {    
            setTimeout(()=>{
                resolve(this.mockLabelDetail(label.id));    
            }, 1500)        
        })
    }
}

module.exports = {
    "MockFetcher": MockFetcher
}