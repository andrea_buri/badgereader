const {CardReader} = require("./card_reader");

class CardReaderRegistry{
    constructor(){

    }
    detect(){
        return Promise.resolve([new CardReader()]) 
    }
}

module.exports = {
    CardReaderRegistry
}