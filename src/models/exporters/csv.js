const fs = require("fs")
var endOfLine = require('os').EOL;

class CsvBackend{
    constructor(outFile){
        //this.file = fs.openSync(outFile,"w")
        this.delimiter = ";"
        this.buffer = ""
    }
    export(rows){
        this.buffer = ""
        for(let row of rows){
            this.exportRow(row)
        }
    }
    exportRow(row){
        let line = row.join(this.delimiter) + endOfLine
        //fs.writeSync(this.file, line)
        this.buffer += line
    }
    read(){
        return this.buffer 
    }    
}
module.exports = {
    CsvBackend
};