import { Badge } from "./models/badge"

class CardRegistry{
    constructor(){
        this.be = new Sqlite3Backend("badge.db")
        Badge.set_persister(this.be)
    }
    register(card){
        console.log("registering card", card)
        card.save()
    }
    getByCardNumber(cardNumber){
        let badge = Badge.readByFilter((badge)=>{return badge.badgeNr == cardNumber})
        
        return cards[0]
    }
}

module.exports = {
    CardRegistry
}