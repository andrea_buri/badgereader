const { Label, Pack } = require('./label')
const { Sqlite3Backend } = require('./persister_sqlite3')
const settings = require('electron-settings')
const { MockFetcher } = require('./mocks/fetcher')
const { Fetcher } = require('./fetcher')

let rowables = {}
class BackgroundManager {
    constructor(){
        Label.set_persister(new Sqlite3Backend("mylabel.db"))
        Label.set_exporter(new Sqlite3Backend("mylabel_vista.db")) 
        if (process.argv.includes("--mockbe")){
            this.fetcher = new MockFetcher()
        }
        else{
            this.fetcher = new Fetcher()
        }
        this.invalidateSettings()
    }
    invalidateSettings(){
        let user_settings = settings.getSync("usersettings")
        if (user_settings){
            let host = user_settings["host"] || ""
            host = host.indexOf("http") == 0 ? host : "http://" + host;
            let parsed_host = ""
            try{
                parsed_host = new URL(host)
            }
            catch{
                return
            }
            if (host.split(":").length > 1){
                this.fetcher.setHost(parsed_host.origin)
                //this.fetcher.setPort(parsed_host.port)
            }
            else{
                this.fetcher.setHost(parsed_host.origin)
                //this.fetcher.setPort(80)
            }
            this.fetcher.setUsername(user_settings["username"])
            this.fetcher.setPassword(user_settings["password"])    
            if (user_settings["selected_company"]){
                this.fetcher.setOrganization(user_settings["selected_company"].id)
            }
        }        
    }
    fetchCompanies() {
        return this.fetcher.getCompanies()
    }
    fetchLabelsFromDB() {
        return new Promise((resolve, reject) => {
            try{
                rowables = {}
                let labels = Label.readAll()
                for (let label of labels) {
                    rowables[label.rowable.hash] = label.rowable
                }
                resolve(
                    Object.values(rowables)
                )
            }
            catch (error){
                reject(error)
            }
        })
    }

    fetchLabels() {
        return new Promise((resolve, reject) => {
            // this.fetcher = new Mockthis.fetcher(10)
            rowables = {}
            Label.deleteAll()
            this.fetcher.getLabels().then(raw_labels => {
                for (const raw_label of raw_labels) {
                    let label = Label.fromRaw(raw_label)
                    rowables[label.rowable.hash] = label.rowable
                    label.save()
                }
                resolve(                    
                    Object.values(rowables)
                )
            }).catch((err)=>{
                reject(err)
            })
        })
    }
    fetchLabel(rowable) {
        return new Promise((resolve, reject) => {
            let current_label = rowables[rowable.hash]
            if(!current_label){
                reject(`no label found with hash ${rowable.hash}`)
            }
            this.fetcher.getLabel(current_label.hash).then(raw_label => {
                let updated_label = Label.fromRaw(raw_label)
                updated_label.save()
                rowables[updated_label.rowable.hash] = updated_label.rowable
                resolve(
                    updated_label
                )
            }).catch((err)=>{reject(err)})
        })
    }
    getLabelMeta(){
        return Label.rowable_meta
    }
    exportPack(rowable){
        let parent_label = Label.readByFilter((el)=>
            {
                for(let child of el.children){
                    if(child.rowable.hash == rowable.hash){
                        return true
                    }
                }
                return false;
            }
        )
        if (!parent_label){
            return;
        }
        let pack = parent_label.children.filter((child)=>{
            if(child.rowable.hash == rowable.hash){
                return true
            }
        })[0]
        pack.export()
    }

}

module.exports = {
    BackgroundManager
}