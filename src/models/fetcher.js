const axios = require('axios');
const http = require('http')

class Fetcher {
    constructor() {
        //this.company_id = 1580
    }
    setHost(host) {
        this._cleanToken();
        this.host = host;
    }
    setPort(port) {
        this._cleanToken();
        this.port = port;
    }
    setUsername(username) {
        this._cleanToken();
        this.username = username;
    }
    setPassword(pwd) {
        this._cleanToken();
        this.pwd = pwd;
    }
    setOrganization(id) {
        this._cleanToken();
        this.company_id = id;
    }
    getPath(path){
        let res =  axios.get(`${this.host}${path}`, 
            {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `JWT ${this.token}`,
                }
            }
        )
        .then((response) => {
            //console.log(response)
            return response
        }, (error) => {
            throw(error)
        })            
        return res 
    }
    getCompanies() {
        return this._renewToken().then(() => {
            return this.getPath(`/api/v1/companies/`).then((res)=>{
                return res['data']['results']
            })
        });
    }
    getLabels() {
        return this._renewToken().then(() => {
            return this.getPath(`/api/v1/labels/eu_profile/?company=${this.company_id}&include=main_info,packs`).then((res)=>{
                return res['data']['result']['labels']
            })
        });
    }
    getLabel(labelId) {
        return this._renewToken().then(() => {
            return this.getPath(`/api/v1/labels/${labelId}/eu_profile/?company=${this.company_id}`).then((res)=>{
                return res['data']['result']
            })
        });
    }

    _cleanToken() {
        this.token = "";
    }
    _isTokenExpired() {
        return true;
    }
    _renewToken() {
        if(!this.username || !this.pwd ){
            throw "missing credentials"
        }
        let data = JSON.stringify({
            username: this.username,
            password: this.pwd,
        });
        return axios.post(`${this.host}/api-token-auth/`, data,
            {
                headers: {
                    "Content-Type": "application/json",
                }
        })
        .then((response) => {
            this.token = response.data.token
        });
    }
    _check() {
        return this._renewToken();
    }
}

module.exports = {
    Fetcher
};