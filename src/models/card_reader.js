const fs = require("fs")
class CardReader{
    constructor(){

    }
    read(){
        return fs.readFileSync("./card", {encoding:"utf8"})
    }
    readCard(registry){
        let cardNumber = fs.readFileSync("./card", {encoding:"utf8"})
        return registry.getByCardNumber(cardNumber)
    }
}

module.exports = {
    CardReader
}