const { Label } = require('./label')
const { Fetcher } = require('./fetcher')
const { Persister } = require('./persister')

class MockFetcher extends Fetcher{
    constructor(response_size = 1){
        super()
        this.response_size = response_size
        this.cnt = 0
    }
    mockCompany(){
        this.cnt += 1;
        return {
            "id": this.cnt,
            "name": `Company ${this.cnt}`
        }
    }
    mockLabel(){
        this.cnt += 1;
        return {
            "id": this.cnt,
            "name": `Label ${this.cnt}`,
            "packs": [
                {
                    "id": 1
                },
                {
                    "id": 2
                }
            ]
        }
    }
    mockCompanyList(){
        let companies = []
        for (let index = 0; index < this.response_size; index++) {
            companies.push(this.mockCompany())
        }
        return companies;
    }

    mockLabelList(){
        let labels = []
        for (let index = 0; index < this.response_size; index++) {
            labels.push(this.mockLabel())
        }
        return labels;
    }
    getCompanies(){
        return new Promise(async (resolve, reject) => {            
            resolve(this.mockCompanyList());    
        })
    }
    getLabels(){
        return new Promise(async (resolve, reject) => {            
            resolve(this.mockLabelList());    
        })
    }
}
function test_fetcher(){
    let fetcher = new MockFetcher(2)
    fetcher.getLabels().then(raw_labels => {
        for (const raw_label of raw_labels) {
            let label = Label.fromRaw(raw_label)      
            console.log(label)
            console.log(label.rowable_repr)   
            for (const child of label.children) {
                console.log(child)
                console.log(child.rowable_repr)
            }
        }
    })
}
function test_persist(){
    let fetcher = new MockFetcher(2)
    let persister = new Persister()
    persister.registerStore(Label.persistable_meta)
    Label.set_persister(persister)
    fetcher.getLabels().then(raw_labels => {
        for (const raw_label of raw_labels) {
            let label = Label.fromRaw(raw_label) 
            label.save()
        }
        let labels = Label.readAll()
        console.log(labels)
    })
}
test_persist()