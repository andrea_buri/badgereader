const { Sqlite3Backend } = require('../persister_sqlite3');
const { CsvBackend } = require('../exporters/csv.js');

const { BadgeLog } = require('../badge_log')
const { Badge } = require('../badge_log')

const fs = require("fs")
var assert = require('assert');


// let label = Label.fromRaw (
//     {
//         "id": 3,
//         "raw": "{'name':'pippo'}",
//         "packs": [
//             {"id":200,"raw":{"name":"mimmo2"}}
//         ]
//     }
// )
// let updated_label = Label.fromRaw (
//     {
//         "id": 3,
//         "raw": "{'name':'pippo33'}",
//         "packs": [
//             {"id":300,"raw":{"name":"mimmo2"}},
//             {"id":400,"raw":{"name":"mimmo3"}}
//         ]
//     }
// )
let badge = BadgeLog.fromRaw(
    {
        'time': '2021-02-10T09:16:38.066Z', 'badgeNr':'123'
    }
)

let another_badge = BadgeLog.fromRaw(
    {
        'time': '2021-02-13T09:16:38.066Z', 'badgeNr':'123'
    }
)
let mybadge = Badge.fromRaw(
    {
        "firstName":"mimmo","lastName":"mino","holderNumber":"10","badgeNr":"123"
    }
)

let duplicatemybadge = Badge.fromRaw(
    {
        "firstName":"rino","lastName":"rino","holderNumber":"11","badgeNr":"123"
    }
)
class SQLiteTester{
    constructor(){

    launch(){
        this.launchTest(()=>(this.testBadgeCanBeCreated()))
        this.launchTest(()=>(this.testDuplicateBadheOverridesOld()))
        this.launchTest(()=>(this.testLogCanBeCreated()))
        this.launchTest(()=>(this.testLogCanBeExported()))
    }
    launchTest(cb){
        this.beforeTest()
        cb()
    }
    beforeTest(){
        try{
            fs.unlinkSync("foo.db")
        }
        catch{
        }
        this.be = new Sqlite3Backend("foo.db")
        Badge.set_persister(this.be)
        BadgeLog.set_persister(this.be)
        this.csvbe = new CsvBackend("badgelogs.csv")
    }
    testBadgeCanBeCreated(){
        mybadge.save()
        badge.save()
        another_badge.save()
        assert(Badge.readAll()[0].firstName=="mimmo")
    }
    testDuplicateBadheOverridesOld(){
        mybadge.save()
        duplicatemybadge.save()
        badge.save()
        another_badge.save()
        assert(Badge.readAll()[0].firstName=="rino")
        
    }
    testLogCanBeCreated(){
        badge.save()
        assert(BadgeLog.readAll().length==1)
        assert(BadgeLog.readAll()[0].badgeNr=='123')
    } 
    testLogCanBeExported(){
        mybadge.save()
        badge.save()
        another_badge.save()
        assert(BadgeLog.readAll().length==2)
        let blog = BadgeLog.readAll()[0]
        assert(Badge.readByFilter((badge)=>{return badge.badgeNr == blog.badgeNr})!==undefined)
        this.csvbe.export(
            BadgeLog.readAll().map((log)=>{
                console.log(log.time)
                let badge = Badge.readByFilter((badge)=>{return badge.badgeNr == log.badgeNr})
                return [log.time, log.badgeNr, badge.firstName, badge.lastName, badge.holderNumber]
            })
        )
        console.log(this.csvbe.read())
    }        

}
tester = new SQLiteTester()
tester.launch()