const { Sqlite3Backend } = require('../persister_sqlite3');
const { Label } = require('../label');
const { Pack } = require('../label');
var assert = require('assert');


function getLabel(name = "", pack_name=""){
    return Label.fromRaw (
        {
            "id": 3,
            "name": `${name}`,
            "packs": [
                {"id":200,"name":pack_name}
            ]
        }
    )
}

class LabelTester{
    constructor(){
    }
    launch(){
        this.launchTest(()=>(this.testLabelCanBeCreated()))
        this.launchTest(()=>(this.testLabelCanBeUpdated()))
        this.launchTest(()=>(this.testPackCanBeUpdated()))
    }
    launchTest(cb){
        this.beforeTest()
        cb()
    }
    beforeTest(){
        Label.set_persister(new Sqlite3Backend("foo.db"))        
    }
    testLabelCanBeCreated(){
        let label = getLabel("pippo")
        label.save()
        let res = Label.readAll()
        assert(res.length == 1)
    }
    testLabelCanBeUpdated(){
        let label = getLabel("pippo")
        label.save()
        label = getLabel("pluto")
        label.save()
        let res = Label.readAll()
        assert(res.length == 1)
        assert(res[0].packs.length == 1)
        assert(res[0].packs[0].name==label.packs[0].name)
    }
    testPackCanBeUpdated(){
        let label = getLabel("pippo","pack0")
        label.save()
        let res = Label.readAll()
        assert(res[0].packs[0].name=="pack0")
        label = getLabel("pippo","pack1")
        label.save()
        res = Label.readAll()
        assert(res.length == 1)
        assert(res[0].packs.length == 1)
        assert(res[0].packs[0].name=="pack1")
    }
}

tester = new LabelTester()
tester.launch()