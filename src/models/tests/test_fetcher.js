const { Fetcher } = require('../fetcher');
var assert = require('assert');

class FetcherTester{
    constructor(){
    }
    launch(){
        this.launchTest(()=>(this.testFetchCompanies()))
        this.launchTest(()=>(this.testFetchLabels()))
        this.launchTest(()=>(this.testFetchLabel()))
    }
    launchTest(cb){
        this.beforeTest()
        cb()
    }
    beforeTest(){
        this.user_settings = {
            "host":"http://localhost:8001",
            "port": 8001,
            "username":"andreaburi",
            "password":"Attiva6370",
            "selected_company":{
                "id":1580
            }
        }
        this.fetcher = new Fetcher()
    }
    testFetchCompanies(){
        this.fetcher.setHost(this.user_settings["host"])
        this.fetcher.setPort(this.user_settings["port"])
        this.fetcher.setUsername(this.user_settings["username"])
        this.fetcher.setPassword(this.user_settings["password"])
        this.fetcher.setOrganization(this.user_settings["selected_company"].id)
        this.fetcher.getCompanies().then((companies)=>{            
            assert.ok(companies.length > 0)
        }).catch((reason)=>{
            assert.fail(`${reason}`)
        })
    }

    testFetchLabels(){
        this.fetcher.setHost(this.user_settings["host"])
        this.fetcher.setPort(this.user_settings["port"])
        this.fetcher.setUsername(this.user_settings["username"])
        this.fetcher.setPassword(this.user_settings["password"])
        this.fetcher.setOrganization(this.user_settings["selected_company"].id)
        this.fetcher.getLabels().then((labels)=>{
            assert.ok(labels.length > 0)
        }).catch((reason)=>{
            assert.fail(`${reason}`)
        })
    }
    testFetchLabel(){
        this.fetcher.setHost(this.user_settings["host"])
        this.fetcher.setPort(this.user_settings["port"])
        this.fetcher.setUsername(this.user_settings["username"])
        this.fetcher.setPassword(this.user_settings["password"])
        this.fetcher.setOrganization(this.user_settings["selected_company"].id)
        this.fetcher.getLabel(48138).then((label)=>{
            assert.ok(label['main_info'])
        }).catch((reason)=>{
            assert.fail(`${reason}`)
        })
    }

}

tester = new FetcherTester()
tester.launch()