const { Sqlite3Backend } = require('../persister_sqlite3');
const { Label } = require('../label');
const { Pack } = require('../label');
var assert = require('assert');


function getLabel(name = "", pack_name=""){
    return Label.fromRaw (
        {
            "id": 3,
            "name": `${name}`,
            "packs": [
                {"id":200,"name":pack_name,"sales_name":pack_name.toUpperCase()}
            ]
        }
    )
}

class LabelTester{
    constructor(){
    }
    launch(){
        this.launchTest(()=>(this.testLabelCanBeExported()))
    }
    launchTest(cb){
        this.beforeTest()
        cb()
    }
    beforeTest(){
        Label.set_exporter(new Sqlite3Backend("expo.db"))        
    }
    testLabelCanBeExported(){
        let label = getLabel("pasta","confezione pesta")
        label.export()
    }
}

tester = new LabelTester()
tester.launch()