const { Sqlite3Backend } = require("./persister_sqlite3");



class RefTable{
    constructor(name){
        this.name = name
    }
    toString(){
        return this.name;
    }
}

class KeyMeta{
    constructor(colName){
        this.colName = colName
    }
}

class ForeignKeyMeta{
    constructor(refTable, refColName, colName){
        this.refTable = refTable
        this.refColName = this.refColName
        this.colName = colName
    }
}

class PersistableMeta{
    constructor(refTable, keyMeta, foreignKeyMeta){
        this.refTable = refTable
        this.keyMeta = keyMeta
        this.foreignKeyMetas = foreignKeyMeta ? [foreignKeyMeta] : [];
        this.data_fields = ["raw"]
    }
    get fkeyName(){
        return this.foreignKeyMetas[0].colName
    }
    get fields(){
        return this.data_fields;
    }
    get cols_schema(){
        return this.data_fields.map((el)=>{return  el+' TEXT'}).join(",")
    }
    get cols_list(){
        return this.data_fields.join(",")
    }
    get snails_list(){
        return this.data_fields.map((field)=>{return `@${field}`}).join(",")
    }
    get cols_snails_list(){
        return this.data_fields.map((field)=>{return `${field} = @${field}`}).join(",")
    }
    get cols(){
        if (!this.foreignKeyMetas.length)
            return `${this.keyMeta.colName}, ${this.cols_list}`
        return `${this.keyMeta.colName}, ${this.fkeyName}, ${this.cols_list}`
    }
    get snail(){
        if (!this.foreignKeyMetas.length)
            return `@${this.keyMeta.colName}, ${this.snails_list}`
        return `@${this.keyMeta.colName}, @${this.fkeyName}, ${this.snails_list}`
    }
    get cols_snail(){
        if (!this.foreignKeyMetas.length)
            return `${this.keyMeta.colName} = @${this.keyMeta.colName}, ${this.cols_snails_list}`
        return `${this.keyMeta.colName} = @${this.keyMeta.colName}, ${this.fkeyName} = @${this.fkeyName}, ${this.cols_snails_list}`
    }
}

class ExportableMeta extends PersistableMeta{
    constructor(refTable, keyMeta, foreignKeyMeta, data_fields = null){
        super(refTable, keyMeta, foreignKeyMeta)
        this.data_fields = data_fields || ["raw"]
    }
}

//there's no interface construct in JS/ES6 build a pseudo interface by class aggregation
class Persistable{
    constructor(){}
    initializer ()     {}
    get persistable(){
        return {
            'raw': this.persistable_raw,
            'meta': this.persistable_meta,
            'key': this.persistable_key,
            'fkey': this.persistable_fkey,

        }
    }
    save(){
        throw "subclass should provide property"
    }
    static set_persister(val){
        throw "subclass should provide property"
    }
    get persistable_raw(){
        throw "subclass should provide property"
    }
    get persistable_meta(){
        throw "subclass should provide property"
    }
    get persistable_key(){
        throw "subclass should provide property"
    }
    get persistable_fkey(){
        throw "subclass should provide property"
    }
}
class Exportable extends Persistable{
    constructor(){}
    initializer ()     {}
    get exportable(){
        let d = {
            'raw': this.persistable_raw,
            'meta': this.exportable_meta,
            'key': this.persistable_key,
            'fkey': this.persistable_fkey,
        }
        for(let field of this.exportable_meta.fields){
            try {
                let subfields = field.split("__")
                let v = null;
                for (let subfield of subfields){
                    v = v ? v[subfield]: JSON.parse(this.persistable_raw)[subfield]
                }
                d[field] = v
            } catch (error) {
                d[field] = ""               
            }
        }
        d['raw'] = this.persistable_raw
        return d
    }
}
class Persister{
    constructor(backend){
        if(!backend){
            this.backend = new Sqlite3Backend();
            return
        }
        this.backend = backend;
    }
    setBackend(backend){
        this.backend = backend;
    }
    registerStore(meta){
        if(!this.backend.tableExists(meta.refTable)){
            this.backend.createTable(meta)
        }
    }
    createOrUpdate(persistable){
        this.backend.createOrUpdate(persistable);
    }
    deleteAll(meta){
        return this.backend.deleteAll(meta)
    }
    readAll(meta, builder){
        return this.backend.readAll(meta)
    }
}

module.exports = {
    Persister,
    Persistable,
    PersistableMeta,
    KeyMeta,
    ForeignKeyMeta,
    RefTable,
    Exportable,
    ExportableMeta
}