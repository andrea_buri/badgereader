import { ipcMain } from "electron"
import { Badge } from "./models/badge_log"
import { BadgeLog } from "./models/badge_log"
import { Sqlite3Backend } from './models/persister_sqlite3';
import { CsvBackend } from './models/exporters/csv';

import { CardReader } from './models/card_reader'
import { download } from 'electron-dl';
import { BrowserWindow } from 'electron'; 


let cardReader = new CardReader()
let be = new Sqlite3Backend("badge.db")
let csvBe = new CsvBackend("log.csv")
Badge.set_persister(be)
BadgeLog.set_persister(be)
//init background manager, it's a facade to all background services
ipcMain.handle("checkcard", async (event) => {
    let cardNumber = await cardReader.read();
    let card = Badge.readByFilter((badge)=>{return badge.badgeNr == cardNumber})
    if(!card){
        throw("no card")
    }
    return {
        firstName: card.firstName,
        lastName: card.lastName,
        holderNumber: card.holderNumber,
    }
});

ipcMain.handle("registercard", async (event, formData) => {
    let card = Badge.fromRaw({
        firstName: formData.firstName,
        lastName: formData.lastName,
        holderNumber: formData.companyId,
        badgeNr: await cardReader.read()
    });
    card.save();
    doRead(); //TESTREMOVE
});

async function doRead(){
    let cardNumber = await cardReader.read();
    let card = Badge.readByFilter((badge)=>{return badge.badgeNr == cardNumber})
    if (!card) {
        return;
    }
    console.log("Logging card", cardNumber);
    BadgeLog.fromRaw({
        "time": (new Date()).toISOString(),
        "badgeNr": cardNumber
    }).save()
}
ipcMain.handle("readcard", async (event) => {
    doRead()
});

ipcMain.handle("readingcard", async (event) => {
    return await Promise.resolve(false);
});

ipcMain.handle("showlogs", async (event) => {
    return BadgeLog.readAll().map((log)=>{
        let card = Badge.readByFilter((badge)=>{return badge.badgeNr == log.badgeNr})
        if (!card){
            return undefined
        }
        return{
            daytime: log.time.substring(11,19),
            day: log.time.substring(0,10),
            firstName: card.firstName,
            lastName: card.lastName,
            holderNumber: card.holderNumber,
            isodate: log.time
        }
    })
});

ipcMain.handle('download-csv', async (event) => {
    const win = BrowserWindow.getFocusedWindow();
    console.log("down")
    csvBe.export(
        BadgeLog.readAll().map((log)=>{
            let badge = Badge.readByFilter((badge)=>{return badge.badgeNr == log.badgeNr})
            return [log.time, log.badgeNr, badge.firstName, badge.lastName, badge.holderNumber]
        })
    )
    console.log(await download(
            win, 
            `data:application/octet-stream,${csvBe.read()}`,
            {
                filename: "logs.csv",
                saveAs: true
            }
    ));
    return "ok"
});