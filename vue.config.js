module.exports = {
    pluginOptions: {
      electronBuilder: {
        // List native deps here if they don't work
        externals: ['better-sqlite3'],
        mainProcessWatch: ['src/models/*' ],
      }
    }
  }