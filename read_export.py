#!/usr/bin/python
import sqlite3
import json
conn = sqlite3.connect("expo.db")
c = conn.cursor()
fields_f = open("src/models/export_fields.json")
fields = json.loads(fields_f.read())

def report(table):
    sorted_fields = sorted(fields[table])
    cols = ",".join(sorted_fields)
    c.execute("SELECT {} FROM {}".format(cols, table))
    row = c.fetchone()
    if not row:
        return {}
    row = [el for el in row]
    return json.dumps(dict(zip(sorted_fields, row)),indent=2)

row = report("labels")
print(row)
row = report("packs")
print(row)